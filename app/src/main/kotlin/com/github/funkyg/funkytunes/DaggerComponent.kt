package com.github.funkyg.funkytunes

import com.github.funkyg.funkytunes.network.ChartsFetcher
import com.github.funkyg.funkytunes.network.GithubUpdateChecker
import com.github.funkyg.funkytunes.network.SearchHandler
import com.github.funkyg.funkytunes.network.ThePirateBayAdapter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(FunkyModule::class))
interface DaggerComponent {

    fun inject(app: FunkyApplication)
    fun inject(chartsFetcher: ChartsFetcher)
    fun inject(searchHandler: SearchHandler)
    fun inject(thePirateBayAdapter: ThePirateBayAdapter)
    fun inject(githubUpdateChecker: GithubUpdateChecker)

}