package com.github.funkyg.funkytunes

import android.app.Application
import android.content.res.Configuration
import java.util.*
import javax.inject.Inject

/**
 * Performs initialization for various libraries and HTTP cache.
 */
class FunkyApplication : Application() {

    @Inject lateinit var component: DaggerComponent

    override fun onCreate() {
        super.onCreate()

        // Force Korean language for release builds
        if (!BuildConfig.DEBUG) {
            val locale = Locale("ko")
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
        }

        DaggerDaggerComponent.builder()
                .funkyModule(FunkyModule(this))
                .build()
                .inject(this)
    }
}
