package com.github.funkyg.funkytunes

import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FunkyModule(private val app: FunkyApplication) {

    @Provides
    @Singleton
    fun getVolleyQueue(): RequestQueue {
        return Volley.newRequestQueue(app)
    }
}
